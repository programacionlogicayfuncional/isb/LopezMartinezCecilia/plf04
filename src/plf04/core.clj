(ns plf04.core)
(defn string-E-1
  [xs]
  (letfn [(f [xs x]
            (if (empty? xs)
              (if (and (>= x 1) (<= x 3))
                true
                false)
              (if (= \e (first xs))
                (f (rest xs) (inc x))
                (f (rest xs) x))))]
    (f xs 0)))
(string-E-1 "Hello")
(string-E-1 "Heelle")
(string-E-1 "Heelele")
(string-E-1 "Hll")
(string-E-1 "e")
(string-E-1 "")
(defn string-E-2
  [xs x]
  (letfn [(f [xs acc]
            (if (empty? xs)
              (if (and (>= acc 1) (<= acc 3))
                true
                false)
              (if (= \e (first xs))
                (f (rest xs) (+ acc 1))
                (f (rest xs) acc))))]
    (f xs x)))
(string-E-2 "Hello" 0)
(string-E-2 "Heelle" 0)
(string-E-2 "Heelele" 0)
(string-E-2 "Hll" 0)
(string-E-2 "e" 0)
(string-E-2 "" 0)

(defn string-Times-1
  [xs x]
  (if (or (== x 0) (neg? x))
    ""
    (str xs (string-Times-1 xs (dec x)))))
(string-Times-1 "Hi" 2)
(string-Times-1 "Hi" 3)
(string-Times-1 "Hi" 1)
(string-Times-1 "Hi" 0)
(string-Times-1 "Hi" 5)
(string-Times-1 "Oh Boy! " 2)
(string-Times-1 "x" 4)
(string-Times-1 "" 4)
(string-Times-1 "code" 2)
(string-Times-1 "code" 3)
(defn string-Times-2
  [xs ys]
  (letfn [(f [xs y acc]
            (if (or (empty? xs) (zero? y) (neg? y))
              acc
              (str xs (f xs (dec y) (str acc)))))]
    (f xs ys "")))
(string-Times-2 "Hi" 2)
(string-Times-2 "Hi" 3)
(string-Times-2 "Hi" 1)
(string-Times-2 "Hi" 0)
(string-Times-2 "Hi" 5)
(string-Times-2 "Oh Boy! " 2)
(string-Times-2 "x" 4)
(string-Times-2 "" 4)
(string-Times-2 "code" 2)
(string-Times-2 "code" 3)

(defn front-Times-1
  [xs x]
  (letfn [(f [xs y]
            (if (< (count xs) 3)
              (if (or (zero? y) (neg? y))
                ""
                (str xs (f xs (dec y))))
              (if (or (zero? y) (neg? y))
                ""
                (str (subs xs 0 3) (f xs (dec y))))))]
    (f xs x)))
(front-Times-1 "Chocolate" 2)
(front-Times-1 "Chocolate" 3)
(front-Times-1 "Abc" 3)
(front-Times-1 "Ab" 4)
(front-Times-1 "A" 4)
(front-Times-1 "" 4)
(front-Times-1 "Abc" 0)
(defn front-Times-2
  [xs y z]
  (letfn [(f [xs y acc]
            (if (< (count xs) 3)
              (if (or (zero? y) (neg? y))
                acc
                (f xs (dec y) (str xs acc)))
              (if (or (zero? y) (neg? y))
                acc
                (f xs (dec y) (str (subs xs 0 3) acc)))))]
    (f xs y z)))
(front-Times-2 "Chocolate" 2 "")
(front-Times-2 "Chocolate" 3 "")
(front-Times-2 "Abc" 3 "")
(front-Times-2 "Ab" 4 "")
(front-Times-2 "A" 4 "")
(front-Times-2 "" 4 "")
(front-Times-2 "Abc" 0 "")

(defn count-XX-1
  [cadena]
  (letfn [(f [x z]
            (if (empty? x)
              z
              (if (and (= \x (first x))
                       (= \x (first (rest x))))
                (f (rest x) (inc z))
                (f (rest x) z))))]
    (f cadena 0)))
(count-XX-1 "abcxx")
(count-XX-1 "xxx")
(count-XX-1 "xxxx")
(count-XX-1 "abc")
(count-XX-1 "Hello there")
(count-XX-1 "Hexxo thxxe")
(count-XX-1 "")
(count-XX-1 "Kittens")
(count-XX-1 "Kittensxxx")
(defn count-XX-2
  [cadena]
  (letfn [(f [y z acc]
            (if (empty? y)
              acc
              (if (and (= z (first y))
                       (= z (first (rest y))))
                (f (rest y) z (inc acc))
                (f (rest y) z acc))))]
    (f cadena \x 0)))
(count-XX-1 "abcxx")
(count-XX-1 "xxx")
(count-XX-1 "xxxx")
(count-XX-1 "abc")
(count-XX-1 "Hello there")
(count-XX-1 "Hexxo thxxe")
(count-XX-1 "")
(count-XX-1 "Kittens")
(count-XX-1 "Kittensxxx")

(defn string-Splosion-1
  [x]
  (letfn [(f [y]
            (if (empty? y)
              y
              (apply str (f (subs y 0 (- (count y) 1))) y)))]
    (f x)))
(string-Splosion-1 "Code")
(string-Splosion-1 "abc")
(string-Splosion-1 "ab")
(string-Splosion-1 "x")
(string-Splosion-1 "fade")
(string-Splosion-1 "there")
(string-Splosion-1 "Kitten")
(string-Splosion-1 "Bye")
(string-Splosion-1 "Good")
(string-Splosion-1 "Bad")
(defn string-Splosion-2
  [xs ys]
  (letfn [(f [y acc]
            (if (empty? y)
              acc
              (f (subs y 0 (- (count y) 1)) (str y acc))))]
    (f xs ys)))
(string-Splosion-2 "Code" "")
(string-Splosion-2 "abc" "")
(string-Splosion-2 "ab" "")
(string-Splosion-2 "x" "")
(string-Splosion-2 "fade" "")
(string-Splosion-2 "there" "")
(string-Splosion-2 "Kitten" "")
(string-Splosion-2 "Bye" "")
(string-Splosion-2 "Good" "")
(string-Splosion-2 "Bad" "")

(defn array-123-1
  [xs]
  (letfn [(f [ys]
            (if (and (= 1 (first ys)) (= 2 (first (rest ys))) (= 3 (first (rest (rest ys)))))
              true
              (if (or (empty? ys) (nil? (first (rest ys))) (nil? (first (rest (rest ys)))))
                false
                (f (rest ys)))))]
    (f xs)))
(array-123-1 [1,1,2,3,1])
(array-123-1 [1,1,2,4,1])
(array-123-1 [1,1,2,3,1])
(array-123-1 [1,1,2,1,2,3])
(array-123-1 [1,1,2,1,2,1])
(array-123-1 [1,2,3,1,2,3])
(array-123-1 [1,2,3])
(array-123-1 [1,1,1])
(array-123-1 [1,2])
(array-123-1 [1])
(array-123-1 [])
(defn array-123-2
  [xs]
  (letfn [(f [xs acc]
            (if (or (empty? xs) (nil? (first (rest xs))) (nil? (first (rest (rest xs)))))
              (and acc false)
              (if (>= (count xs) 3)
                (if (and (== (first xs) 1) (== (first (rest xs)) 2) (== (first (rest (rest xs))) 3))
                  (and acc true)
                  (f (rest xs) acc))
                (f (empty xs) acc))))]
    (f xs "")))
(array-123-2 [1,1,2,3,1])
(array-123-2 [1,1,2,4,1])
(array-123-2 [1,1,2,3,1])
(array-123-2 [1,1,2,1,2,3])
(array-123-2 [1,1,2,1,2,1])
(array-123-2 [1,2,3,1,2,3])
(array-123-2 [1,2,3])
(array-123-2 [1,1,1])
(array-123-2 [1,2])
(array-123-2 [1])
(array-123-2 [])

(defn string-X-1
  [xs]
  (letfn [(f [y z]
            (if (empty? y)
              ""
              (if (== 0 z)
                (str (first y) (f (rest y) (inc z)))
                (if (and (= \x (first y)) (> (count y) 1))
                  (f (rest y) (inc z))
                  (str (first y) (f (rest y) (inc z)))))))]
    (f xs 0)))
(string-X-1 "xxHxix")
(string-X-1 "abxxxcd")
(string-X-1 "xabxxxcdx")
(string-X-1 "xKittenx")
(string-X-1 "Hello")
(string-X-1 "xx")
(string-X-1 "x")
(string-X-1 "")
(defn string-X-2
  [xs]
  (letfn [(f [x y acc]
            (if (empty? x)
              acc
              (if (<= y 0)
                (str (first x) (f (rest x) (inc y) (str acc)))
                (if (and (= \x (first x)) (> (count x) 1))
                  (f (rest x) (inc y) (str acc))
                  (str (first x) (f (rest x) (inc y) acc))))))]
    (f xs 0 "")))
(string-X-2 "xxHxix")
(string-X-2 "abxxxcd")
(string-X-2 "xabxxxcdx")
(string-X-2 "xKittenx")
(string-X-2 "Hello")
(string-X-2 "xx")
(string-X-2 "x")
(string-X-2 "")

(defn alt-Pairs-1
  [x]
  (letfn [(f [y z]
            (if (== (count y) z)
              ""
              (if (or (== z 0) (== z 1) (== z 4) (== z 5) (== z 8) (== z 9))
                (str (subs y z (+ z 1)) (f x (inc z)))
                (f x (inc z)))))]
    (f x 0)))
(alt-Pairs-1 "kitten")
(alt-Pairs-1 "Chocolate")
(alt-Pairs-1 "CodingHorror")
(alt-Pairs-1 "yak")
(alt-Pairs-1 "ya")
(alt-Pairs-1 "")
(alt-Pairs-1 "ThisThatTheOther")
(defn alt-Pairs-2
  [xs]
  (letfn [(f [y z acc]
            (if (== (count y) z)
              acc
              (if (or (== z 0) (== z 1) (== z 4) (== z 5) (== z 8) (== z 9))
                (f y (inc z) (str acc (subs y z (+ z 1))))
                (f y (inc z) acc))))]
    (f xs 0 "")))
(alt-Pairs-2 "kitten")
(alt-Pairs-2 "Chocolate")
(alt-Pairs-2 "CodingHorror")
(alt-Pairs-2 "yak")
(alt-Pairs-2 "ya")
(alt-Pairs-2 "")
(alt-Pairs-2 "ThisThatTheOTher")

(defn string-Yak-1
  [xs]
  (letfn [(f [y z]
            (if (empty? y)
              ""
              (if (and (= (first y) \y) (= (first (rest y)) \a) (= (first (rest (rest y))) \k) (> (count y) 1))
                (f (rest (rest (rest y))) (inc z))
                (str (first y) (f (rest y) (inc z))))))]
    (f xs 0)))
(string-Yak-1 "yakpak")
(string-Yak-1 "pakyak")
(string-Yak-1 "yak123ya")
(string-Yak-1 "yak")
(string-Yak-1 "yakxxxyak")
(string-Yak-1 "HiyakHi")
(string-Yak-1 "xxxyakyyyakzzz")
(defn string-Yak-2
  [x]
  (letfn [(f [y z acc]
            (if (empty? y)
              acc
              (if (and (= (first y) \y) (= (first (rest y)) \a) (= (first (rest (rest y))) \k) (> (count y) 1))
                (f (rest (rest (rest y))) (inc z) acc)
                (f (rest y) (inc z) (str acc (first y))))))]
    (f x 0 "")))
(string-Yak-2 "yakpak")
(string-Yak-2 "pakyak")
(string-Yak-2 "yak123ya")
(string-Yak-2 "yak")
(string-Yak-2 "yakxxxyak")
(string-Yak-2 "HiyakHi")
(string-Yak-2 "xxxyakyyyakzzz")

(defn has-271-1
  [xs]
  (letfn [(f [ys]
            (if (and (= 2 (first ys)) (= 7 (first (rest ys))) (= 1 (first (rest (rest ys)))))
              true
              (if (or (empty? ys) (nil? (first (rest ys))) (nil? (first (rest (rest ys)))))
                false
                (f (rest ys)))))]
    (f xs)))
(has-271-1 [1, 2, 7, 1])
(has-271-1 [1, 2, 8, 1])
(has-271-1 [2,7,1])
(has-271-1 [3,8,2])
(has-271-1 [2, 7, 3])
(has-271-1 [2,7,4])
(has-271-1 [2,7,-1])
(has-271-1 [4, 5, 3, 8, 0])
(has-271-1 [2, 7, 5, 10, 4])
(has-271-1 [2, 7, -2, 4, 9, 3])
(has-271-1 [2, 7, 5, 10, 1])
(has-271-1 [2, 7, -2, 4, 10, 2])
(has-271-1 [1, 1, 4, 9, 0])
(has-271-1 [1, 1, 4, 9, 4, 9, 2])
(defn has-271-2
  [xs]
  (letfn [(f [xs acc]
            (if (or (empty? xs) (nil? (first (rest xs))) (nil? (first (rest (rest xs)))))
              (and acc false)
              (if (>= (count xs) 3)
                (if (and (== (first xs) 2) (== (first (rest xs)) 7) (== (first (rest (rest xs))) 1))
                  (and acc true)
                  (f (rest xs) acc))
                (f (empty xs) acc))))]
    (f xs "")))
(has-271-2 [1, 2, 7, 1])
(has-271-2 [1, 2, 8, 1])
(has-271-2 [2,7,1])
(has-271-2 [3,8,2])
(has-271-2 [2, 7, 3])
(has-271-2 [2,7,4])
(has-271-2 [2,7,-1])
(has-271-2 [4, 5, 3, 8, 0])
(has-271-2 [2, 7, 5, 10, 4])
(has-271-2 [2, 7, -2, 4, 9, 3])
(has-271-2 [2, 7, 5, 10, 1])
(has-271-2 [2, 7, -2, 4, 10, 2])
(has-271-2 [1, 1, 4, 9, 0])
(has-271-2 [1, 1, 4, 9, 4, 9, 2])